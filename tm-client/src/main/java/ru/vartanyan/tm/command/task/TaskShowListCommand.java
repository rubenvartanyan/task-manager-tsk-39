package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.Task;
import ru.vartanyan.tm.enumerated.Sort;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        List<Task> list;
        list = endpointLocator.getTaskEndpoint().findAllTasks(session);
        int index = 1;
        for (@Nullable final Task task: list) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
