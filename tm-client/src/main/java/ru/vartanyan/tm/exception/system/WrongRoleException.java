package ru.vartanyan.tm.exception.system;

public class WrongRoleException extends Exception {

    public WrongRoleException() throws Exception {
        super("Error! You role doesn't support this command");
    }

}
