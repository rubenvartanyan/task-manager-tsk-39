package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
