package ru.vartanyan.tm.enumerated;

import ru.vartanyan.tm.comparator.ComparatorByCreated;
import ru.vartanyan.tm.comparator.ComparatorByDateStarted;
import ru.vartanyan.tm.comparator.ComparatorByName;
import ru.vartanyan.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by create date", ComparatorByCreated.getInstance()),
    DATE_STARTED("Sort by init date", ComparatorByDateStarted.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }
}
